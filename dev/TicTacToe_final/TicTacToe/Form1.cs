﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace TicTacToe
{
    public partial class Form1 : Form
    {
        public int felderanzahl = 3; //Spielbrettgroesse veranedern: HIER
        TableLayoutPanel tlp = new TableLayoutPanel();
        Spiel l = new Spiel(3); //Spielbrettgroesse veranedern: HIER
        public Form1()
        {
            InitializeComponent();
        }
        public void CreateFeld(int anz)
        {
            tlp.Controls.Clear();
            tlp.Width = 600;
            tlp.Height = 600;

            tlp.Location = new System.Drawing.Point(150, 150);

            tlp.ColumnCount = anz;
            tlp.RowCount = anz;

            this.Controls.Add(tlp);
            for (int i = 0; i < tlp.RowCount * tlp.ColumnCount; i++)
            {
               PictureBox pb = new PictureBox();
               pb.Click += panel_Click;
               pb.BackColor = Color.Black;
               pb.Width = 95;
               pb.Height = 95;
               pb.SizeMode = PictureBoxSizeMode.StretchImage;
               tlp.Controls.Add(pb);
            }
        }
        void panel_Click(object sender, EventArgs e)
        {
            var pp = tlp.GetPositionFromControl(sender as PictureBox);

            switch (l.Clicked(pp.Column, pp.Row))
            {
                case 0: MessageBox.Show("Bitte wählen sie ein leeres Feld aus");
                    break;
                case 1: (sender as PictureBox).Image = Image.FromFile("p1.png");
                   
                    if (l.Win() == true)
                    {
                        MessageBox.Show("Spieler 1 (Patriots) hat gewonnen!");
                    }
                    break;

                case 2: (sender as PictureBox).Image = Image.FromFile("p2.png");

                    if (l.Win())
                    {
                        MessageBox.Show("Spieler 2 (Broncos) hat gewonnen!");
                    }
                    break;
            }
        }
        private void button1_Click(object sender, EventArgs e)
        {
            CreateFeld(felderanzahl);
            l.clearArray();
        }

        private void optionsToolStripMenuItem_Click(object sender, EventArgs e)
        {
        }
    }
}
