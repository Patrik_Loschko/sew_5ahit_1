﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Bankenanwendung
{
    class Program
    {
        static void Main(string[] args)
        {
            Konto k1 = new Konto
            {
                Betrag = 100,
                Inhaber = new Person { Vorname = "Herwig", Familienname = "Macho" },
                Nr = "AT01 6546 1236 4477 3669"
            };
            Konto k2 = new Konto
            {
                Betrag = 1000,
                Inhaber = new Person { Vorname = "Susi", Familienname = "Macho" },
                Nr = "AT99 6546 4776 3321 1234"
            };
            Konto k3 = new Konto
            {
                Betrag = 500,
                Inhaber = new Person { Vorname = "Susi", Familienname = "Meier" },
                Nr = "AT45 3665 2334 1145 0002"
            };

            Spaarkonto k4 = new Spaarkonto
            {
                Betrag = 500,
                Inhaber = new Person { Vorname = "Patrik", Familienname = "Loschko" },
                Nr = "AT45 3665 2334 1145 4567"
            };

            Gehaltskonto k5 = new Gehaltskonto
            {
                Betrag = 500,
                Inhaber = new Person { Vorname = "Erduan", Familienname = "Ismani" },
                Nr = "AT45 3665 2334 1145 9999"
            };

            Bank Raika = new Bank { Bezeichnung = "Raika Krems", Kunden = new List<Konto>() { k2, k1, k3, k4, k5 } };
            Console.WriteLine(Raika);
            Raika.Abschluss();
            Console.WriteLine(Raika);
            Raika.Sort(); // Sortieren mit IComparable von Konto
            Raika.Kunden.Sort();
            Console.WriteLine(Raika);
            Raika.Kunden.Sort( // Sortieren über delegaten und Kontonummer
                delegate(Konto a, Konto b)
                {
                    return a.Nr.CompareTo(b.Nr);
                });
            Console.WriteLine(Raika);
            Raika.Ueberweisung(30, k1, k2);
            k5.Abhebung(10);
            k4.Abhebung(100);
            Console.WriteLine(Raika);
            Console.ReadLine();
        }

    }
}
