﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Bankenanwendung
{
    class Bank
    {
        public string Bezeichnung  { get; set; }
        public List<Konto> Kunden = new List<Konto>();

        public Konto this[int index]
        {
            get { return Kunden[index]; }
            set { Kunden[index] = value; }
        }

        public void Abschluss() 
        {
            foreach (Konto k in Kunden)
            {
                k.Betrag += (k.Betrag * Konto.Zinssatz);
            }
        }
        public void Add(Konto neu)
        {
            Kunden.Add(neu);
        }
        public void Add(List<Konto> neu)
        {
            this.Kunden = neu;
        }
        public void Sort() 
        {
            Kunden.Sort();
        }
        public override string ToString()
        {
            string s = "";
            foreach (Konto k in Kunden)
            {
                s += Bezeichnung + "\nInhaber: " + k.Inhaber.Vorname + " " + k.Inhaber.Familienname + "\nKontostand: " + k.Betrag + "\nNr: " + k.Nr + "\n\n";
            }
            return s+"\n\n\n";
        }
        public void Ueberweisung(double betrag, Konto k1, Konto k2)
        {
            k1.Betrag = k1.Betrag - betrag;
            k2.Betrag = k2.Betrag + betrag;
        }
    }
    class Konto : IComparable
    {
        public double Betrag { get; set; }
        public string Nr { get; set; }
        internal static double Zinssatz { get; set; }
        public Person Inhaber { get; set; }

        public virtual void Abhebung(double betrag)
        {
            Betrag = Betrag + betrag;
        }
        public int CompareTo(object other)
        {
            return 0;
        }
        public void Einzahlung(double betrag)
        {
            Betrag = Betrag + betrag;
        }
        public Konto()
        {
        }
        public override string ToString()
        {
            return base.ToString();
        }
        internal static void Ueberweisung(double betrag, Konto k1, Konto k2)
        {
            k1.Betrag = k1.Betrag - betrag;
            k2.Betrag = k2.Betrag + betrag;
        }
        public static void Zinsen(double neu) 
        {
            Zinssatz = neu;
        }
    }
    class Gehaltskonto : Konto
    {
        public double rahmen { get; set; }//double rahmen = -1000;//überziehungsramen sollte 1000 sein

        public override void Abhebung(double betrag)
        {
            double neuBetrag = Betrag - betrag;

            if (neuBetrag < rahmen)
            {
                Betrag = Betrag;
                Console.WriteLine("Konto kann nicht so weit überzogen werden");
            }
            else
            {
                Betrag = neuBetrag;
            }
        }

        double bonussatz = 0.01; //zinssatz
        public void BerechneZinssatz()
        {
            Betrag *= (1 + bonussatz);
        }
    }
    class Spaarkonto : Konto
    {
        public double rahmen {get;set;}//double rahmen = 0;//überziehungsramen sollte 0 sein

        public override void Abhebung(double betrag)
        {
            double neuBetrag = Betrag - betrag;

            if (neuBetrag < rahmen)
            {
                Betrag = Betrag;
                Console.WriteLine("Konto kann nicht so weit überzogen werden");
            }
            else
            {
                Betrag = neuBetrag;
            }
        }

        double bonussatz = 0.05; // höherer Zinnsatz bei spaarkonto
        public void BerechneZinssatz()
        {
            Betrag *= (1 + bonussatz);
        }
    }
    class Person : IComparable
    {
        public string Familienname { get; set; }
        public string Vorname { get; set; }
        
        List<int> dummyprivate = new List<int>();
        List<double> dummypotected = new List<double>();
        List<string> dummypublic = new List<string>();

        public int CompareTo(object other) 
        {
            return 0;
        }
        public override string ToString()
        {
            return base.ToString();
        }
    }
}
