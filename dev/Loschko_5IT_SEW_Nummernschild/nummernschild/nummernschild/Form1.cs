﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace nummernschild
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        private void Form1_Paint(object sender, PaintEventArgs e)
        {
            int heigth = 100;//100
            int width = 250;//250
            int startx = 50;//50
            int starty = 50;//50

            Pen Pen = new Pen(Color.White);//weisse flaeche
            Pen.Width = 1;
            e.Graphics.DrawRectangle(Pen, startx - 5, starty - 5, width + 15, heigth + 15);
            e.Graphics.FillRectangle(Brushes.White, new Rectangle(startx - 5, starty - 5, width + 15, heigth + 15));

            Pen Pen1 = new Pen(Color.Red);//Rote Linien
            Pen1.Width = 1;
            e.Graphics.DrawLine(Pen1, startx, starty, startx + width, startx);
            e.Graphics.DrawLine(Pen1, startx, starty + 3, startx + width, startx + 3);

            e.Graphics.DrawLine(Pen1, startx, startx + heigth, startx + width, startx + heigth);
            e.Graphics.DrawLine(Pen1, startx, startx + heigth + 3, startx + width, startx + heigth + 3);

            Pen Pen2 = new Pen(Color.Blue);//blaue flaeche
            Pen2.Width = 1;
            e.Graphics.DrawRectangle(Pen2, startx + 3, starty + 5, startx - 15, startx + 42);
            e.Graphics.FillRectangle(Brushes.Blue, new Rectangle(startx + 3, starty + 5, startx - 15, startx + 42));

            //Weisses A in blauer flaeche
            Font Font = new Font("Times New Roman", 32);
            e.Graphics.DrawString("A", Font, Brushes.White, startx - 2, starty + 50);

            e.Graphics.DrawString("KR", Font, Brushes.Black, startx + 3 + 30, starty + 50 - 20);//Schwarzes KR
            e.Graphics.DrawString("578IL", Font, Brushes.Black, startx + 3 + 65 + 75, starty + 50 - 20);//Schwarzes 578IL

            e.Graphics.DrawImage(Image.FromFile("img.png"), startx + 100, starty + 10 + 5,startx, starty + 25);
        }

        private void Form1_Load(object sender, EventArgs e)
        {
            
        }
    }
}
