﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Sockets;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Xml.Linq;

namespace Servere
{
    class Program
    {
        static TcpListener listener;
        const int LIMIT = 5;

        public static void Main()
        {
            new Thread(new ThreadStart(() =>
            {
                    Console.Title = "Server";
            })).Start();

            listener = new TcpListener(IPAddress.Any, 2000);
            listener.Start();
            for (int i = 0; i < LIMIT; i++)
            {
                Thread t = new Thread(new ThreadStart(doit));
                t.Start();
            }
        }

        public static void doit()
        {

                Socket so = listener.AcceptSocket();

                Stream s = new NetworkStream(so);
                StreamWriter sw = new StreamWriter(s);
                StreamReader sr = new StreamReader(s);

                sw.AutoFlush = true;
                string request = sr.ReadLine();


                sw.Write(CreateTree(request).ToString());
                sw.Close();

                s.Close();


                so.Close();
        }
        static XElement CreateTree(string source)
        {
            DirectoryInfo di = new DirectoryInfo(source);
            var v = new XElement("Dir", new XAttribute("Name", di.Name),
            from d in Directory.GetDirectories(source) select CreateTree(d), from fi in di.GetFiles()
            select new XElement("File", new XAttribute("Name", fi.Name)));
            return v;
        }
    }
}
