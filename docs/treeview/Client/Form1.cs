﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Net.Sockets;
using System.IO;
using System.Xml.Linq;
using System.Xml;

namespace Client
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        private void Button1_Click(object sender, EventArgs e)
        {
            TcpClient client = new TcpClient("127.0.0.1", 2000);
           
                Stream s = client.GetStream();

                StreamReader sr = new StreamReader(s);
                StreamWriter sw = new StreamWriter(s);

                sw.AutoFlush = true;
                
                sw.WriteLine(textBox1.Text);
                TreeNode node = new TreeNode();
                XElement xe = XElement.Parse(sr.ReadToEnd());

                node = ToTreeNode(xe);
                fileTreeView.Nodes.Add(node);
            }

        public static TreeNode ToTreeNode(XElement xe)
        {
            return new TreeNode(xe.Name+": "+xe.Attribute("Name").Value, xe.Elements().Select(e => ToTreeNode(e)).ToArray());
        }

        private void fileTreeView_AfterSelect(object sender, TreeViewEventArgs e)
        {
            textBox2.Text = fileTreeView.SelectedNode.FullPath.ToString();
        }
    }
}